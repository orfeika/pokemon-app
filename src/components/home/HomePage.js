import React from "react";

const HomePage = () => {
  return (
    <div>
      <h2>Welcome to site about your favorite Pokémons</h2>
      <p> Use our site to find out more about Pokémons</p>
    </div>
  );
};

export default HomePage;
